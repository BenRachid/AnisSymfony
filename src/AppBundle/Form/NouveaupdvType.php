<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NouveaupdvType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('raisonSociale')
            ->add('typePdv')
            ->add('adressePdv')
            ->add('wilayaPdv')
            ->add('communePdv')
            ->add('msisdn')
            ->add('autreTelephonePdv')
            ->add('emailPdv')
            ->add('password')
            ->add('codeVendeur')
            ->add('statue')
            ->add('dateCreation', 'datetime')
            ->add('dateModification', 'datetime')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Nouveaupdv'
        ));
    }
}
