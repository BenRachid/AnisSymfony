<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Nouveaupdv
 *
 * @ORM\Table(name="nouveaupdv")
 * @ORM\Entity
 */
class Nouveaupdv
{
    /**
     * @var integer
     *
     * @ORM\Column(name="pdv", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pdv;

    /**
     * @var string
     *
     * @ORM\Column(name="raison_sociale", type="string", length=10000, nullable=true)
     */
    private $raisonSociale;

    /**
     * @var string
     *
     * @ORM\Column(name="type_pdv", type="string", length=10000, nullable=true)
     */
    private $typePdv;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse_pdv", type="text", length=65535, nullable=true)
     */
    private $adressePdv;

    /**
     * @var string
     *
     * @ORM\Column(name="wilaya_pdv", type="string", length=10000, nullable=false)
     */
    private $wilayaPdv;

    /**
     * @var string
     *
     * @ORM\Column(name="commune_pdv", type="string", length=10000, nullable=true)
     */
    private $communePdv;

    /**
     * @var integer
     *
     * @ORM\Column(name="msisdn", type="integer", nullable=true)
     */
    private $msisdn;

    /**
     * @var integer
     *
     * @ORM\Column(name="autre_telephone_pdv", type="integer", nullable=true)
     */
    private $autreTelephonePdv;

    /**
     * @var string
     *
     * @ORM\Column(name="email_pdv", type="string", length=10000, nullable=true)
     */
    private $emailPdv;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=50, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="code_vendeur", type="string", length=50, nullable=true)
     */
    private $codeVendeur;

    /**
     * @var string
     *
     * @ORM\Column(name="Statue", type="string", length=10000, nullable=false)
     */
    private $statue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_creation", type="datetime", nullable=true)
     */
    private $dateCreation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modification", type="datetime", nullable=true)
     */
    private $dateModification;


    /**
     * @var integer
     */
    private $id;


    /**
     * Set raisonSociale
     *
     * @param string $raisonSociale
     * @return Nouveaupdv
     */
    public function setRaisonSociale($raisonSociale)
    {
        $this->raisonSociale = $raisonSociale;

        return $this;
    }

    /**
     * Get raisonSociale
     *
     * @return string 
     */
    public function getRaisonSociale()
    {
        return $this->raisonSociale;
    }

    /**
     * Set typePdv
     *
     * @param string $typePdv
     * @return Nouveaupdv
     */
    public function setTypePdv($typePdv)
    {
        $this->typePdv = $typePdv;

        return $this;
    }

    /**
     * Get typePdv
     *
     * @return string 
     */
    public function getTypePdv()
    {
        return $this->typePdv;
    }

    /**
     * Set adressePdv
     *
     * @param string $adressePdv
     * @return Nouveaupdv
     */
    public function setAdressePdv($adressePdv)
    {
        $this->adressePdv = $adressePdv;

        return $this;
    }

    /**
     * Get adressePdv
     *
     * @return string 
     */
    public function getAdressePdv()
    {
        return $this->adressePdv;
    }

    /**
     * Set wilayaPdv
     *
     * @param string $wilayaPdv
     * @return Nouveaupdv
     */
    public function setWilayaPdv($wilayaPdv)
    {
        $this->wilayaPdv = $wilayaPdv;

        return $this;
    }

    /**
     * Get wilayaPdv
     *
     * @return string 
     */
    public function getWilayaPdv()
    {
        return $this->wilayaPdv;
    }

    /**
     * Set communePdv
     *
     * @param string $communePdv
     * @return Nouveaupdv
     */
    public function setCommunePdv($communePdv)
    {
        $this->communePdv = $communePdv;

        return $this;
    }

    /**
     * Get communePdv
     *
     * @return string 
     */
    public function getCommunePdv()
    {
        return $this->communePdv;
    }

    /**
     * Set msisdn
     *
     * @param integer $msisdn
     * @return Nouveaupdv
     */
    public function setMsisdn($msisdn)
    {
        $this->msisdn = $msisdn;

        return $this;
    }

    /**
     * Get msisdn
     *
     * @return integer 
     */
    public function getMsisdn()
    {
        return $this->msisdn;
    }

    /**
     * Set autreTelephonePdv
     *
     * @param integer $autreTelephonePdv
     * @return Nouveaupdv
     */
    public function setAutreTelephonePdv($autreTelephonePdv)
    {
        $this->autreTelephonePdv = $autreTelephonePdv;

        return $this;
    }

    /**
     * Get autreTelephonePdv
     *
     * @return integer 
     */
    public function getAutreTelephonePdv()
    {
        return $this->autreTelephonePdv;
    }

    /**
     * Set emailPdv
     *
     * @param string $emailPdv
     * @return Nouveaupdv
     */
    public function setEmailPdv($emailPdv)
    {
        $this->emailPdv = $emailPdv;

        return $this;
    }

    /**
     * Get emailPdv
     *
     * @return string 
     */
    public function getEmailPdv()
    {
        return $this->emailPdv;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return Nouveaupdv
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set codeVendeur
     *
     * @param string $codeVendeur
     * @return Nouveaupdv
     */
    public function setCodeVendeur($codeVendeur)
    {
        $this->codeVendeur = $codeVendeur;

        return $this;
    }

    /**
     * Get codeVendeur
     *
     * @return string 
     */
    public function getCodeVendeur()
    {
        return $this->codeVendeur;
    }

    /**
     * Set statue
     *
     * @param string $statue
     * @return Nouveaupdv
     */
    public function setStatue($statue)
    {
        $this->statue = $statue;

        return $this;
    }

    /**
     * Get statue
     *
     * @return string 
     */
    public function getStatue()
    {
        return $this->statue;
    }

    /**
     * Set dateCreation
     *
     * @param \DateTime $dateCreation
     * @return Nouveaupdv
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return \DateTime 
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set dateModification
     *
     * @param \DateTime $dateModification
     * @return Nouveaupdv
     */
    public function setDateModification($dateModification)
    {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification
     *
     * @return \DateTime 
     */
    public function getDateModification()
    {
        return $this->dateModification;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
}
