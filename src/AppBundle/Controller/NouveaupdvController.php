<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Nouveaupdv;
use AppBundle\Form\NouveaupdvType;

/**
 * Nouveaupdv controller.
 *
 * @Route("/nouveaupdv")
 */
class NouveaupdvController extends Controller
{
    /**
     * Lists all Nouveaupdv entities.
     *
     * @Route("/", name="nouveaupdv_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $nouveaupdvs = $em->getRepository('AppBundle:Nouveaupdv')->findAll();

        return $this->render('nouveaupdv/index.html.twig', array(
            'nouveaupdvs' => $nouveaupdvs,
        ));
    }

    /**
     * Creates a new Nouveaupdv entity.
     *
     * @Route("/new", name="nouveaupdv_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $nouveaupdv = new Nouveaupdv();
        $form = $this->createForm('AppBundle\Form\NouveaupdvType', $nouveaupdv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($nouveaupdv);
            $em->flush();

            return $this->redirectToRoute('nouveaupdv_show', array('id' => $nouveaupdv->getId()));
        }

        return $this->render('nouveaupdv/new.html.twig', array(
            'nouveaupdv' => $nouveaupdv,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Nouveaupdv entity.
     *
     * @Route("/{id}", name="nouveaupdv_show")
     * @Method("GET")
     */
    public function showAction(Nouveaupdv $nouveaupdv)
    {
        $deleteForm = $this->createDeleteForm($nouveaupdv);

        return $this->render('nouveaupdv/show.html.twig', array(
            'nouveaupdv' => $nouveaupdv,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Nouveaupdv entity.
     *
     * @Route("/{id}/edit", name="nouveaupdv_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Nouveaupdv $nouveaupdv)
    {
        $deleteForm = $this->createDeleteForm($nouveaupdv);
        $editForm = $this->createForm('AppBundle\Form\NouveaupdvType', $nouveaupdv);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($nouveaupdv);
            $em->flush();

            return $this->redirectToRoute('nouveaupdv_edit', array('id' => $nouveaupdv->getId()));
        }

        return $this->render('nouveaupdv/edit.html.twig', array(
            'nouveaupdv' => $nouveaupdv,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Nouveaupdv entity.
     *
     * @Route("/{id}", name="nouveaupdv_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Nouveaupdv $nouveaupdv)
    {
        $form = $this->createDeleteForm($nouveaupdv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($nouveaupdv);
            $em->flush();
        }

        return $this->redirectToRoute('nouveaupdv_index');
    }

    /**
     * Creates a form to delete a Nouveaupdv entity.
     *
     * @param Nouveaupdv $nouveaupdv The Nouveaupdv entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Nouveaupdv $nouveaupdv)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('nouveaupdv_delete', array('id' => $nouveaupdv->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
